/*
  
  ---- TABLE OF CONTENTS ----

  
*/
  
/*
    ---- NOTES ----
    Please do NOT use console.log();
    Please do NOT try to log out anything.
    There is a really weird bug with logging you will be here all day tring to figure out the issue.
*/
  
  
// 0. Engineering Principles of Systems in Bubble
// 
// I. Utility Methods
// 	a. Daily-run functions
//	b. Minute-run functions
// X. Deprecated Methods


/**
 * methodNameHere
 * 
 * Write a short few sentences on what this function does. If there were 
 * specific engineering details or ideas used, list them here! ;)
 *
 * @param type - variableName
 * @param type - variableName2
 * @return response - the type of response callback from this function
 */

/**
 * deleteOldNotification
 * 
 * Deletes all notifications after 24 hours of being on the server. This
 * ensures that users don't receive notifications that are over 24 hours 
 * old. 
 *
 * @return response - a response with the status of the job's completion
 */
Parse.Cloud.job("deleteOldNotification", function(request, response) {

	Parse.Cloud.useMasterKey();

	var Notifications = Parse.Object.extend("Notification"); //Creates a new Notification object
	var query = new Parse.Query(Notifications); //Define the query to query for Notification class on the server
	var day = new Date(); //Create a new date that is set to today
    //Set our query date constraint           
  	day.setDate(day.getDate()-1); //Set the day to yesterday, this will be used to find out how old objects are
  	query.lessThan("createdAt", day); //Define the query to query for objects created less than (before) yesterday
  	query.limit = 1000; //Limit the number of objects this query can bring back to 1000. This is overriding the Parse default of 100.
      
	//Destroy the queried objects
  	query.find({
    	success:function(results) {
             //Iteratively traverse the list of returned objects and destroy them from the database                                 
            for (var i = 0, len = results.length; i < len; i++) {
                var result = results[i];
                result.destroy({});
            }
			//Respond with success that the objects were destroyed and the number of objects that were destroyed
            response.success("Removed " + results.length + " notifications");                  
        },
        error:function(results, error) {
            response.error(error.message);
        }
  });

});

/**
 * deleteOldMessage
 * 
 * Deletes all messages after 24 hours of being on the server. This
 * ensures that users don't receive messages that are over 24 hours 
 * old. We do this on servers
 *
 * @return response - a response with the status of the job's completion
 */
Parse.Cloud.job("deleteMessages", function(request, response) {

	Parse.Cloud.useMasterKey();
	
	var Message = Parse.Object.extend("Message"); //Creates a new Message object
	var query = new Parse.Query(Message); //Define the query to query for Notification class on the server
	var day = new Date(); //Create a new date that is set to today

	//Calls to delete the last message from the head message. It dereferences the head message from a message object so that references aren't broken.
	Parse.Cloud.run('deleteLastMessageFromHead', {}, {
	  success: function(success) {
	  },
	  error: function(result, error) {
	  }
	});

	//Set our query date constraint 
	day.setDate(day.getDate()-1); //Set the day to yesterday, this will be used to find out how old objects are
  	query.lessThan("createdAt", day); //Define the query to query for objects created less than (before) yesterday
  	query.limit = 1000; //Limit the number of objects this query can bring back to 1000. This is overriding the Parse default of 100.

	query.find({
		success:function(results) {
			//Iteratively traverse the list of returned objects and destroy them from the database
			for (var i = 0, len = results.length; i < len; i++) {
                var result = results[i];
                result.destroy({});
            }
            //Respond with success that the objects were destroyed and the number of objects that were destroyed
            response.success("Removed " + results.length + " messages");
		},
		error:function(results, error) {
			response.error(error.message);
		}
	});
});

/**
 * deleteLastMessageFromHead
 * 
 * Deletes the message thread head pointer in a message object from the Message object.
 * This solves any pointer resolution errors where a message head references a null message object.
 *
 * @return response - a response with the status of the job's completion
 */
Parse.Cloud.define("deleteLastMessageFromHead", function(request, response) {

	Parse.Cloud.useMasterKey();
	var MessageHead = Parse.Object.extend("MessageHead");
	var query = new Parse.Query(MessageHead);
	var day = new Date();
	day.setDate(day.getDate()-1);

	query.lessThan("lastMessageDate", day);
	query.limit = 1000;
	query.find({
		success:function(results) {

			for (var i = 0, len = results.length; i < len; i++) {
                var result = results[i];
                result.destroy({});
            }

            response.success("Removed " + results.length + " message heads");

		},
		error:function(results, error) {
			response.error(error.message);
		}

	});

});

/**
 * setPendingBuddy
 * 
 * Sets a pending Buddy on for a user that has requested that user as a Buddy. 
 *
 * @param username - the username of the user that is to be requested 
 * @param requestingUsername - the current username
 * @return response - a response with the status of the job's completion
 */
Parse.Cloud.define("setPendingBuddy", function(request, response) {
	
	Parse.Cloud.useMasterKey();
  
	var username = request.params.username; //The user the Buddy request will be sent to
	var requestingUsername = request.params.requestingUser; //The current User's username
	var pendingBudddies = []; //Create a new array to hold the previous pending buddies (if applicable) or initialize the array for the first Buddy request
	
	var query = new Parse.Query(Parse.User); //Creates a new User object
	var currentUser = request.user; //Gets the current User object
	var objectId = currentUser.id; //Get the current User objectId

	query.equalTo("username", username); //Set our query username constraint to the user that the Buddy request will be sent to
	query.first({
	   	success: function(user) {

			pendingBuddies = user.get("pendingBuddies"); //Gets the pending Buddies array
  
		   	if (pendingBuddies === null || pendingBuddies === undefined) {
				pendingBuddies = [objectId]; //If the pending Buddies array has no contents, instantiate it with the objectId to 
		   	} else {
				pendingBuddies.splice(0, 0, objectId); //If it isn't empty, add it to the front of the list (order descending by newest Buddy request)
		   	}
   
		   	user.set("pendingBuddies", pendingBuddies); //Set pending Buddies as the array we just manipulated
	  
	  		//Save the User object that we just modified with the new Pending Buddy
			user.save(null, {
			success: function(result) {
				//Push a notification to the user of the Buddy request
				Parse.Push.send({
					channels: [username],
					data: {
					alert: requestingUsername + " " + "has requested you as a Buddy!",
					category: "buddyRequest"
					}
					}, {
						success: function() {
					},
						error: function(error) {
					}
				});
				//Sets a notification object for the user
				Parse.Cloud.run('setNotification', { buddy: username, currentUsername: requestingUsername, nType: "BuddyRequest"}, {
					success: function(success) {
						response.success("Success");
					},
					error: function(error) {
					}
				}); 
				},
			error: function(error) {
			}
			});
		},
		error: function() {
			response.error("Failed to add the pending Buddy");
		}
	});
});

/**
 * acceptBuddy
 * 
 * Sets a pending Buddy on for a user that has requested that user as a Buddy. 
 *
 * @param username - the username of the user that is to be requested 
 * @param requestingUsername - the current username
 * @return response - a response with the status of the job's completion
 */   
Parse.Cloud.define("acceptBuddy", function(request, response) {
                   Parse.Cloud.useMasterKey();
                      
                   var username = request.params.username;
                   var currentUsername = request.params.currentUser;
                   var currentUser = request.user;
                   var query = new Parse.Query(Parse.User);
                   var didAdd = false;

                   query.equalTo("username", username);
                   query.first({
                               success: function(user) {
                                  
                               // Adding buddy to the requesting user
                               var userBuddiesArray = user.get("buddies");
                                  
                               if (userBuddiesArray == null) {
                              	 userBuddiesArray = [currentUser.id];
                               } else if (userBuddiesArray.indexOf(currentUser.id) == -1) {
                              	 userBuddiesArray.splice(0, 0, currentUser.id);
                              	 didAdd = true;
                               }
                                  
                               user.set("buddies", userBuddiesArray);
                                  
                               var points = user.get("points");
                               points++;
                               user.set("points", points);
                                  
                               user.save(null, {
                                         success: function(result) {
                                         
                                         if (didAdd) {
                                         	
    										Parse.Cloud.run("pushNotification", {channel : [username], alert : currentUsername + " " + "has just accepted your Buddy Request!", sound : "Pop.aiff", category : "buddyRequest"}, {
                                              success: function(result) {
                                               
                                              },
                                              error: function(error) {
                                                
                                              }
                                            });

    										/*
                                         	Parse.Push.send({
                                                         channels: [username],
                                                         data: {
                                                         alert: currentUsername + " " + "has just accepted your Buddy Request!"
                                                         }
                                                         }, {
                                                         success: function() {
                                                         // Push was successful
                                                         },
                                                         error: function(error) {
                                                         // Handle error
                                                         }
                                                         });
                                         	*/

                                         }
                                            
                                         //response.success("Success");
                                         },
                                         error: function(error) {
                                            
                                         }
                                         });
										
										var requestedUserQuery = new Parse.Query(Parse.User);
                      
					                     requestedUserQuery.equalTo("username", currentUsername);
					                     requestedUserQuery.first({
					                                            success: function(user2) {
					                                               
					                                            var userBuddiesArray2 = user2.get("buddies");
					                                               
					                                            if (userBuddiesArray2 == null) {
					                                            	userBuddiesArray2 = [user.id];
					                                            } else if (userBuddiesArray2.indexOf(user.id) == -1){
					                                            	userBuddiesArray2.splice(0, 0, user.id);
					                                            }
					                                               
					                                            user2.set("buddies", userBuddiesArray2);
					                                               
					                                            var userPendingBuddiesArray2 = user2.get("pendingBuddies");
					                                            var index = userPendingBuddiesArray2.indexOf(user.get("objectId"));
					                                               
					                                            userPendingBuddiesArray2.splice(index, 1);
					                                               
					                                            user.set("pendingBuddies", userPendingBuddiesArray2);
					                                               
					                                            var points = user2.get("points");
					                                            points++;
					                                               
					                                            user2.set("points", points);
					                                               
					                                            user2.save(null, {
					                                                      success: function(result) {
					                                                         
					                                                     	 response.success("Success");
					                                                      },
					                                                      error: function(error) {
					                                                         
					                                                      }
					                                                      });
					                                               
					                                            },
					                                            error: function() {
					                                            //response.error("Failed");
					                                            }
					                                               
					                                            });
                                  
                               },
                               error: function() {
                               //response.error("Failed");
                               }
                               });
   				      
});
   
Parse.Cloud.define("denyBuddy", function(request, response) {
                     
                   Parse.Cloud.useMasterKey();
                      
                   var username = request.params.username;
                   var currentUser = request.user;
                      
                   var requestedUserQuery = new Parse.Query(Parse.User);
                      
                   requestedUserQuery.equalTo("username", username);
                   requestedUserQuery.first({
                                            success: function(user) {
                                               
                                            var userPendingBuddiesArray = currentUser.get("pendingBuddies");
                                               
                                            var index = userPendingBuddiesArray.indexOf(user.get("objectId"));
                                               
                                            userPendingBuddiesArray.splice(index, 1);
                                               
                                            currentUser.set("pendingBuddies", userPendingBuddiesArray);
                                               
                                            currentUser.save(null, {
                                                      success: function(result) {
                                                         
                                                      response.success("Success");
                                                      },
                                                      error: function(error) {
                                                         
                                                      }
                                                      });
                                               
                                            },
                                            error: function() {
                                            response.error("Failed");
                                            }
                                               
                                            });
                      
});
   
Parse.Cloud.define("removeBuddy", function(request, response) {
                   Parse.Cloud.useMasterKey();
                      
                   var userToRemove = request.params.userToRemove;
                   var currentUser = request.user;
                   var buddies = [];
                   var buddies2 = [];
                   var query = new Parse.Query(Parse.User);
                   var query2 = new Parse.Query(Parse.User);
                      
                   query.equalTo("username", userToRemove);
                   query.first({
                               success: function(userResult1) { 
                                  
                               buddies = userResult1.get("buddies");
                                  
                               var index = buddies.indexOf(currentUser.id);
                                  
                               buddies.splice(index, 1);
                                  
                               userResult1.set("buddies", buddies);
                                  
                               var points = userResult1.get("points");
                               points--;
                                  
                               userResult1.set("points", points);
                                  
                               userResult1.save(null, {
                                         success: function(result) {
                                         
                                         	buddies2 = currentUser.get("buddies");
                                   
                               				var index = buddies2.indexOf(userResult1.id);
                                   
                                			buddies2.splice(index, 1);
                                   
                               				currentUser.set("buddies", buddies2);
                                   
                               				var points = currentUser.get("points");
                               				points--;
                                   
                               				currentUser.set("points", points);
                                   
                               				currentUser.save(null, {
                                        		  success: function(result) {
                                             
                                         		 	response.success("Success");
                                         		 },
                                         		 error: function(error) {
                                             
                                         		 }
                                         	});
                                         
                                         },
                                         error: function(error) {
                                            
                                         }
                                         });
                                  
                               },
                               error: function() {
                               //response.error("Failed");
                               }
                               });
                 
});
   
Parse.Cloud.define("searchUser", function(request, response) {
  
  var searchString = request.params.searchString;
  
  // Start query for users that start with searchString
  
  var query = new Parse.Query(Parse.User);
  
  query.startsWith("username", searchString);
  query.find({
    
  success: function(results) {
      
    // Return queried array of usernames.

    response.success(results);
      
  },
    
  error: function(error) {

  	response.error(error);
    
  }
  
  });
  
});
  
Parse.Cloud.define("popPoints", function(request, response) {
  
  Parse.Cloud.useMasterKey();
  
  //This is the all-seeing function in Bubble. This function calculates the difficulty rating for the popularity system in Bubble. 
  //It is based on a Sigmoid-type function and based on the work of Sadaf Mackertich and Doug Woodrow.
  
  //Last updated: 12/9/2014
  
  //Parse Query for the number of points for the user
  var c = 1000;
  var currentUser = request.user;
  var maxDistance = request.params.bounds;
  var points = currentUser.get("points");
  var currentLocation = currentUser.get("location");
  var query = new Parse.Query(Parse.User);
  var queryNumber = new Parse.Query(Parse.User);
  query.withinMiles("location", currentLocation, maxDistance);
  query.count({
      success: function(count) {
        var currentUserBuddies = currentUser.get("buddies");
        var queryFriendsNearby = new Parse.Query(Parse.User);
        queryFriendsNearby.withinMiles("location", currentLocation, maxDistance);
        queryFriendsNearby.containedIn("objectId", currentUserBuddies);
        queryFriendsNearby.count({
          success: function(countFriends) {
              var popPoints = c*points/(1000*count/countFriends + points);
              currentUser.set("popPoints", popPoints);
              currentUser.save();
              response.success("Success");
          },
          error: function(error) {
            // Failed to count friends
            response.error(error);
          }
        });
      },
      error: function(error) {
        // The request failed
        //console.log("Error finding the people around.");
        response.error(error);
      }
  });
      //Get the count of number of people around you
});
  
  
// IV. Sign-up Utilities
Parse.Cloud.define("addPoints", function(request, response) {
                   Parse.Cloud.useMasterKey();
                      
                   var username = request.params.username;
                   var query = new Parse.Query(Parse.User);
                      
                   query.equalTo("username", username);
                   query.first({
                               success: function(user) {
                                  
                               console.log(user);
                                  
                               user.set("points", 10);
                               user.set("staff", false);
                                  
                               user.save(null, {
                                         success: function(result) {
                                            
                                         response.success("Success");
                                         },
                                         error: function(error) {
                                            
                                         }
                                         });
                                  
                               },
                               error: function() {
                               response.error("Failed");
                               }
                    });
                      
});
  
Parse.Cloud.define("likePost", function(request, response) {
  
  Parse.Cloud.useMasterKey();
  
  var currentUsername = request.user.get("username");
  var currentUser = request.user;
  var objectId = request.params.objectId;
  var currentNumberOfLikes = 0;
  var post = Parse.Object.extend("Post");
  var postQuery = new Parse.Query(post);
  var LIKE_NOTIFICATION = "Like";
  var notification = Parse.Object.extend("Notification");
  var notificationQuery = new Parse.Query(notification);
  var queriedNotification;
  var likers = [];
  var hasLikedBefore = [];
  var notificationObject;
  
        // Get the post to like
        postQuery.equalTo("objectId", objectId);
        postQuery.find({
  
        success: function(result) {
             
            var queriedPost = result[0];
             
            currentNumberOfLikes = queriedPost.get("likes");
            currentNumberOfLikes++;
  
            var points = queriedPost.get("points");
  
            points++;
  
            queriedPost.set("points", points);
  
            likers = queriedPost.get("likers");
  
            if (likers === null || likers === undefined) {
  
                likers = [currentUsername];
  
            } else {
  
                likers.push(currentUsername);
            }
  
            queriedPost.set("likes", currentNumberOfLikes);
            queriedPost.set("likers", likers);
              
            hasLikedBefore = queriedPost.get("hasLikedBefore");
              
            if (hasLikedBefore === null || hasLikedBefore === undefined) {
  
                hasLikedBefore = [currentUsername];
                queriedPost.set("hasLikedBefore", hasLikedBefore);
  
            } else {
  
                var likedBefore = hasLikedBefore.indexOf(currentUsername);
                  
                if (likedBefore == -1) {
  
                  hasLikedBefore.push(currentUsername);
                  queriedPost.set("hasLikedBefore", hasLikedBefore);
  
                }
                        
            }
             
            queriedPost.save(null, {
                                        success: function(savedPost) {
                                              
                                            Parse.Cloud.run("setNotification", {pObject : objectId, nType : "Like"}, {
                                              success: function(result) {
                                                response.success();
                                              },
                                              error: function(error) {
                                                response.error("Successfully saved post. Failed to save notification.");
                                              }
                                            });
  
                                            },
                                            error: function(error) {
                                                response.error(error);
                                            }
  
                                          });
                                           
             },
             error: function(error) {
                console.log("FAILING");
                response.error(error);
             }
        });
  
});
  
Parse.Cloud.define("unlikePost", function(request, response) {
  
  Parse.Cloud.useMasterKey();
  
  var currentUser = request.user;
  var objectId = request.params.objectId;
  var post = Parse.Object.extend("Post");
  var postQuery = new Parse.Query(post);
  var likers = [];
  
  postQuery.equalTo("objectId", objectId);
  postQuery.find({
  
    success: function(results) {
  
      post = results[0];
        
      var likes = post.get("likes");
      likes--;
  
      var points = post.get("points");
  
      points--;
  
      post.set("points", points);
  
      post.set("likes", likes);
  
      likers = post.get("likers"); 
  
      var index = likers.indexOf(currentUser.get("username"));
  
      if (index > -1) {
        likers.splice(index, 1);
      } 
  
      post.set("likers", likers);
      post.save({
  
        success: function(success) {
          response.success("Unliked complete");
        },
         error: function(error) {
          response.error(error);
         }
  
      });
  
    },
     error: function(error) {
  
      response.error(error);
     }
  
  });
  
});
  
Parse.Cloud.define("setNotification", function(request, response) {
  
    Parse.Cloud.useMasterKey();
  
    var notificationO = Parse.Object.extend("Notification");
    var p = Parse.Object.extend("Post");
    var postQuery = new Parse.Query(p);
    var post = request.params.pObject;
    var type = request.params.nType;
    var postObjectGlobal;
  	
  	if (type == "BuddyRequest") {

  		var notification = new notificationO();
  		var buddy = request.params.buddy;
                      
                            notification.set("sendTo", request.params.buddy);
                            notification.set("sentFrom", request.params.currentUsername);
                            notification.set("type", type);
                      
                            notification.save(null, {
  
                                success: function(savedNotification) {
                                    response.success("Did save new notification");
                                 },
                                 error: function(error) {
                                    response.error(error);  
                                 }
  
                                });

  	} else {

  		postQuery.get(post, {
  
        success: function(postObject) {
  
                postObjectGlobal = postObject;
  				
  				var notification = new notificationO();
          
                notification.set("sendTo", postObjectGlobal.get("authorUsername"));
                notification.set("sentFrom", request.user.getUsername());
                notification.set("post", postObjectGlobal);
                notification.set("type", type);
          
                notification.save(null, {

                    success: function(savedNotification) {
                        response.success("Did save new notification");
                     },
                     error: function(error) {
                        response.error(error);  
                     }

                });

        },
         error: function(error) {
                response.error(error);
        }       
  
    });

  	}

  
});
  
Parse.Cloud.define("pushNotification", function(request, response) {
  
    var channel = request.params.channel;
    var alert = request.params.alert;
    var sound = request.params.sound;
    var category = request.params.category;
    var requesting = request.params.requestingUser;
    var view = request.params.viewToPush;
  	var userQuery = new Parse.Query(Parse.User);

	if (Object.prototype.toString.call( channel ) === '[object Array]') {
	      
	      	userQuery.equalTo("username", channel[0]);
	      	userQuery.find({
	      		success: function(results) {
				    var user = results[0];
				    var notificationTypes = user.get("notifications");
				    var currentType = notificationTypes[category];
				    
				    if (currentType == true) {

				    	Parse.Push.send({
					      channels: channel,
					      data: {
					        alert: alert,
					        badge: "Increment",
					        sound: sound,
					        category: category,
					        requestingUser: requesting,
					        viewToPush: view
					      }
					     }, 
					      {
					      success: function(success) {
					        response.success("Successfully pushed");
					      },
					       error: function(error) {
					        response.error(error);
					     }
					  });

				    } else {
				    	response.success("User has " + currentType + " notifications off.");
				    }

				  },
				  error: function(error) {
				    
				  }
	      	});

		  } else {
		  
		    Parse.Push.send({
		  
		      channels: [ channel ],
		      data: {
		        alert: alert,
		        badge: "Increment",
		        sound: sound,
		        category: category,
		        requestingUser: requesting,
		        viewToPush: view
		      }
		     }, 
		      {
		      success: function(success) {
		        response.success("Successfully pushed");
		      },
		       error: function(error) {
		        response.error(error);
		     }
		    });
	  
  		  }
  
});
  
Parse.Cloud.define("postStatus", function(request, response) {
  
    // Parameters
    var shouldSavePhoto = request.params.shouldSavePhoto;
    var text = request.params.text;
    var data = request.params.file;
    var file = new Parse.File("imageFile.jpg", data);
    var point = request.params.point;
    var locationName = request.params.locationName;
    var mentions = request.params.mentions;
    var pointOfInterest = request.params.pointOfInterest;
    var pointOfInterestName = request.params.pointOfInterestName;
    var pointOfInterestGeoPoint = request.params.pointOfInterestGeoPoint;
    var currentUser = request.user;
    var userProfilePic = currentUser.get("profilePicture");
	var postClass = Parse.Object.extend("Post");
    var post = new postClass();
    var hashtag;
  	var buffer = text.split("#");
  	buffer = buffer[1];

  	if (buffer === null || buffer === undefined) {
  	} else {

    	hashtag = buffer.split(" ");
   	 	hashtag = hashtag[0];
   	 	hashtag = hashtag.toLowerCase();
   	 	post.set("hashtag", hashtag);
  	}

    post.set("text", text);
    post.set("authorUsername", currentUser.getUsername());
    post.set("author", currentUser);
    post.set("likes", 0);
    post.set("profilePic", userProfilePic);
    post.set("points", currentUser.get("popPoints"));
    post.set("location", point);
    post.set("locationName", locationName);
    post.set("mentioned", mentions);
    post.set("comments", 0);
    post.set("authorId", currentUser.id);

    if (pointOfInterest != "" && pointOfInterestName != "") {
		post.set("locationName", pointOfInterestName);   
	}

    post.save(null, {
  
        success: function(result) {
            
            var numberPosts = currentUser.get("posts");
            numberPosts++;
            currentUser.set("posts", numberPosts);
            currentUser.save();
            
            //If any of the pointOfInterest data fields are not set, we don't need to add a location. This makes it easy for users to post semi-anonymously.
	          if (pointOfInterest != "" && pointOfInterestName != "") {
	              Parse.Cloud.run('addPostToLocation', { placeId: pointOfInterest, geoPoint: pointOfInterestGeoPoint, placeName: pointOfInterestName, postToAdd: post.id }, {
	                  success: function(result) {
	                    
	                  },
	                  error: function(error) {
	                  }
	            });
	          }

            if (file === null || file === undefined) {
  
            } else {
  
                post.set("photo", file);
  				
  				if (shouldSavePhoto == true) {
                	
                	var BSPhoto = Parse.Object.extend("Photo");
 				    var bsPhoto = new BSPhoto();
  					
  					bsPhoto.set("user", currentUser);
  					bsPhoto.set("photo", file);
  					bsPhoto.set("post", post.id);
  					bsPhoto.set("tags", []);
  					bsPhoto.set("comments", []);
  					bsPhoto.save(null, {
  						success: function(result) {
                     	
                    	}, 
                    	error: function(error) {
                    	
                   	 	}
  					});

                }

                post.save(null, {
                    success: function(result) {
                     	response.success("success");
                    }, 
                    error: function(error) {
                    	response.success("success");
                    }
                });
  
            }
        }, 
        error: function(error) {
            response.error(error);
        }
  
    });
  
});
 
Parse.Cloud.define("phoneVerification", function(request, response) {
 
    // Require and initialize the Twilio module with your credentials
    var client = require('twilio')('AC2efa76c2f87cf58a4c471ace9efb3f2b', '71cfcea72fee7d717d71ad074e9e3adb');
 
    var usPrefix = '+1';
    var phoneNumber = request.params.phoneNumber;
    phoneNumber = usPrefix.concat(phoneNumber);
 
    var messageString = "Welcome to Bubble; your're almost there! Here is your code: ";
 
    var code = Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000;
 
    var message = messageString.concat(code);
 
    var verificationClass = Parse.Object.extend("Phone");
    var verification = new verificationClass();
 
    // Save the number and the code
    verification.set("phoneNumber", phoneNumber);
    verification.set("verificationCode", code);
    verification.set("verified", false);
 
    verification.save(null, {
 
        success: function(result) {
 
            // Send an SMS message
            client.sendSms({
 
                to: phoneNumber, 
                from: '+14072698066',
                body: message
 
              }, function(error, responseData) { 
 
                if (error) {
                  response.error(error);
                } else { 
                    response.success("Successfully sent verification text!");
                }
              }
            );
 
        },
        error: function(error) {
            response.error(error);
        }
 
    });
 
});

Parse.Cloud.define("addPostToLocation", function(request, response) {

  var Locations = Parse.Object.extend("Location");
  var query = new Parse.Query(Locations);
  query.equalTo("placeId", request.params.placeId);
  
  query.find({
    success: function(results) {
      //If the object has a location associated with it, we need to add a post object to the array that contains all posts at that location
      if (results.length != 0) {
        results[0].set("placeId", request.params.placeId);
        results[0].set("geoPoint", request.params.geoPoint);
        results[0].set("name", request.params.placeName);

        var numberOfPastPosts = results[0].get("count");
        numberOfPastPosts++;
        results[0].set("count", numberOfPastPosts);

        var pastPosts = results[0].get("posts");
        pastPosts.push(request.params.postToAdd);
        results[0].set("posts", pastPosts);

        results[0].save(null, {
          success: function(success) {
            response.success("success");
          },
           error: function(error) {
            
           }
    
        });
      }
      //Else we will create a new location from the past placeId and location's data
      else {
         
        //Create a new location object to create the first post at the location
        var newLocation = new Locations();

        newLocation.set("placeId", request.params.placeId);
        newLocation.set("geoPoint", request.params.geoPoint);
        newLocation.set("name", request.params.placeName);
        newLocation.set("count", 1);

        var pastPosts = [request.params.postToAdd];
        newLocation.set("posts", pastPosts);

        newLocation.save(null, {
          success: function(success) {
            response.success("success");
          },
           error: function(error) {
            
           }
    
        });
      }
    },
    //Even though you would think this is where the new location should be created if not found, it isn't, this is a Parse shortcoming
    error: function(error) {
      
    }
  });
 
});

// Should be called everytime a message needs to be sent
// Will send pushNotification from within the code (not yet implemented)
Parse.Cloud.define("sendMessage", function(request, response) {

	// Parameters
	// Contatenate the objectId's of all the people in convseration
	// with a "." between each (Must be done in the client and sent)
	var chatId = request.params.chatId;
	// Array of user ObjectId's
	var userIdArray = chatId.split(".");

	// New user added to conversation
	var flag = request.params.updateChatId; // Boolean
	
	var messageText = request.params.messageText;
	var data = request.params.picture;
	var file = new Parse.File("imageFile.jpg", data);
	var shareLocation = request.params.shareLocation;
	var sender = currentUser.get("objectId");

	// Get all usernames in loop and contenate with "."
	// remove current user username
	var query = new Parse.Query("_User");
	query.containedIn("objectId",userIdArray);
	// First query finds all usernames
	query.find({
		success: function(results) {
			// Contenate Strings "ObjectId1.ObjectId2.ObjectId3..."
			var userNameString = results[0].getUsername;
				for (i = 1; i = userIdArray.length; i++){
					userNameString = userNameString.concat(".",results[i]);
				}

				var messageUserClass = Parse.Object.extend("MessageUser");
				var messageUser = new messageUserClass();


				// Set messageUserClass
				messageUser.set("lastMessage", messageText);
				messageUser.set("user",sender);
				messageUser.addUnique("talkingTo", chatId);
				messageUser.addUnique("talkingTo", userNameString);
				messageUser.save(null, {
					// Save to messageUser
			        success: function(resultUser) {
							      response.success("success");

							      var messageHolderClass = Parse.Object.extend("MessageHolder");
								  var messageHolder = new messageHolderClass();

								  messageHolder.set("chatId",chatId);
								  messageHolder.set("messageText",messageText);
								  messageHolder.set("sender", sender);

								  // Save to messageHolder
								  messageHolder.save(null, {
								  	success: function(resultSave) {
									  	if (file !== null || file !== undefined &&
									  		shareLocation !== null || shareLocation !== undefined) {

									  		file.save().then(function() {

									   		 }, function(error) {

									   	 	});

										    messageHolder.set("picture", file);
										    messageHolder.set("shareLocation", shareLocation);
										    messageHolder.save(null, {

				    						success: function(result) {
				      							response.success("success");
				      							// Push to User

										    }, 
										    error: function(error) {
										      	response.success("success");
										    }
										});

										} else if (file !== null || file !== undefined &&
									  		shareLocation === null || shareLocation === undefined) {

											file.save().then(function() {

									   		 }, function(error) {

									   	 	});

										    messageHolder.set("picture", file);
										    messageHolder.set("shareLocation", shareLocation);

										    messageHolder.save(null, {

					    						success: function(result) {
					      							response.success("success");
											    }, 
											    error: function(error) {
											      	response.success("success");
											    }
											});


										} else if (file === null || file === undefined &&
									  		shareLocation !== null || shareLocation !== undefined) {

										    messageHolder.set("shareLocation", shareLocation);

										    messageHolder.save(null, {

					    						success: function(result) {
					      							response.success("success");
											    }, 
											    error: function(error) {
											      	response.success("success");
											    }
											});


										} else {
											// Do Nothing For Now
										}
								  		
								  	},
								  	error: function(error2) {
								  		response.error("error2")
								  	}
								  });

			        }, 
			        error: function(error) {
			          	response.error(error);
			        }

			    });

		},
		error: function() {
			// Do nothing 
		}
	});

});

Parse.Cloud.define("getMessage", function(request, response) {
	var chatIdArray = request.params.chatId;
	var limitNumber = request.params.limitNumber;
	var query = new Parse.Query("MessageHolder");
	// Return all messages that match the chatId's in the Array
	query.containedIn("objectId",chatIdArray);
	query.limit(limitNumber);
	query.find({
		success: function(result) {
			// Return all messages
		},
		error: function(error) {
			// Do some crap
		}
	});

});


Parse.Cloud.define("postComment", function(request, response) {

  // Parameters
  var postId = request.params.postID;
  var text = request.params.text;
  var data = request.params.file;
  var file = new Parse.File("imageFile.jpg", data);
  var locationName = request.params.locationName;
  var currentUser = request.user;
  var userProfilePic = currentUser.get("ProfilePic");
  
  // For future feature add likes to comments so users can like
  // comments
  var commentClass = Parse.Object.extend("Comment");
  var comment = new commentClass();

  comment.set("comment", text);
  comment.set("commenter", currentUser.getUsername());
  comment.set("profilePic", userProfilePic);
  comment.set("locationName", locationName);

  comment.save(null, {

        success: function(result) {
          
      if (file === null || file === undefined) {

      } else {

        
          comment.set("photo", file);

          comment.save(null, {

            success: function(result) {
              response.success("success");
            }, 
            error: function(error) {
              response.success("success");
            }
        });

      }

            //response.success("success");
        }, 
        error: function(error) {
            response.error(error);
        }

    });

});

Parse.Cloud.define("aroundMe", function(request, response) {

  var userLocation = request.params.location;

  var query = new Parse.Query(Parse.User);
  var maxDistance = request.params.bounds;
  query.withinMiles("location", userLocation, maxDistance);
  query.find().then(function(results){
  // randomize results
  var currentInd = results.length;
  var tempVal;
  var randInd;

  while (0 !== currentInd) {

    randInd = Math.floor(Math.random()*currentInd);
    currentInd -= 1;

    tempVal = results[currentInd];
    results[currentInd] = results[randInd];
    results[randInd] = tempVal;
  }

  // return array of randomized people
  return results;

  },function(error){
    // Do some error handling
  });

});

//X. Deprecated Methods
/*
Parse.Cloud.job("deleteOldEntries", function(request, status) {

	Parse.Cloud.useMasterKey();

	var Post = Parse.Object.extend("Post");
	var query = new Parse.Query(Post);
	var day = new Date();
  	day.setDate(day.getDate()-1);
	
  	query.lessThan("createdAt", day);
  	query.limit = 1000;

	query.find({
		success:function(results) {

			if (results.length == 0) {
				status.success("There are 0 posts to delete");
			}

			// Run through posts that are 24 hours old, return those posts that are > 24 hours old. By > 24 hours we nean literally ANYTHING over 24 hours.
			for (var i = 0; i < results.length; i++) {

				var postBackup = Parse.Object.extend("Post_Backup");
				var PostBackup = new postBackup();
				var result = results[i];

				// Copy data into post backup class
				PostBackup.set("postObjectId", result.id);
				PostBackup.set("author", result.get("author"));
				PostBackup.set("authorUsername", result.get("authorUsername"));
				PostBackup.set("commenters", result.get("commenters"));
				PostBackup.set("comments", result.get("comments"));
				PostBackup.set("photo", result.get("photo"));
				PostBackup.set("likers", result.get("likers"));
				PostBackup.set("likes", result.get("likes"));
				PostBackup.set("location", result.get("location"));
				PostBackup.set("locationName", result.get("locationName"));
				PostBackup.set("text", result.get("text"));
				PostBackup.set("createdAt", result.get("createdAt"));

				PostBackup.save(null, {
					success: function(stat) {

					//status.success("Successfully deleted objects");

					},
					error: function(post, error) {
					  status.error(error.message + " Couldn't save backup");
					}
				});

				// Delete the result after copying
				result.destroy({});

				if (i == results.length) {
					status.success("Successfully deleted " + results.length + " objects");
				}

			}

		},
		error: function(error) {
		  status.error(error.message + " Didn't query");
		}
	});
                   
});

*/

